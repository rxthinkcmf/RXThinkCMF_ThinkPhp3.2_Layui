## 📚 项目介绍

一款 PHP 语言基于 Yii2.x、Layui、MySQL等框架精心打造的一款模块化、插件化、高性能的前后端分离架构敏捷开发框架，可用于快速搭建前后端分离后台管理系统，本着简化开发、提升开发效率的初衷，框架自研了一套个性化的组件，实现了可插拔的组件式开发方式：单图上传、多图上传、下拉选择、开关按钮、单选按钮、多选按钮、图片裁剪等等一系列个性化、轻量级的组件，是一款真正意义上实现组件化开发的敏捷开发框架，框架已集成了完整的RBAC权限架构和常规基础模块，同时支持多主题切换，可以根据自己喜欢的风格选择想一个的主题，实现了个性化呈现的需求；

为了敏捷快速开发，提升研发效率，框架内置了一键CRUD代码生成器，自定义了模块生成模板，可以根据已建好的表结构(字段注释需规范)快速的一键生成整个模块的所有代码和增删改查等等功能业务，真正实现了低代码开发，极大的节省了人力成本的同时提高了开发效率，缩短了研发周期，是一款真正意义上实现组件化、低代码敏捷开发框架。

## 🍻 环境要求:

* PHP >= 7.4(推荐：7.4)
* PDO PHP Extension
* MBstring PHP Extension
* CURL PHP Extension
* 开启静态重写
* 要求环境支持pathinfo

## 🍪 内置模块
+ 用户管理：用于维护管理系统的用户，常规信息的维护与账号设置。
+ 角色管理：角色菜单管理与权限分配、设置角色所拥有的菜单权限。
+ 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
+ 职级管理：主要管理用户担任的职级。
+ 岗位管理：主要管理用户担任的岗位。
+ 部门管理：主要管理系统组织架构，对组织架构进行统一管理维护。
+ 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
+ 登录日志：系统登录日志记录查询包含登录异常。
+ 字典管理：对系统中常用的较为固定的数据进行统一维护。
+ 配置管理：对系统的常规配置信息进行维护，网站配置管理功能进行统一维护。
+ 城市管理：统一对全国行政区划进行维护，对其他模块提供行政区划数据支撑。
+ 友链管理：对系统友情链接、合作伙伴等相关外链进行集成维护管理的模块。
+ 个人中心：主要是对当前登录用户的个人信息进行便捷修改的功能。
+ 广告管理：主要对各终端的广告数据进行管理维护。
+ 站点栏目：主要对大型系统网站等栏目进行划分和维护的模块。
+ 会员管理：对各终端注册的会员进行统一的查询与管理的模块。
+ 网站配置：对配置管理模块的数据源动态解析与统一维护管理的模块。
+ 通知公告：系统通知公告信息发布维护。
+ 代码生成：一键生成模块CRUD的功能，包括后端和前端等相关代码。
+ 案例演示：常规代码生成器一键生成后的演示案例。

## 👷 软件信息
* 软件名称：Yii2Admin 敏捷开发框架 Layui 版
* 演示地址：[http://manage.yii2.layui.rxthink.cn](http://manage.yii2.layui.rxthink.cn)

账号 | 密码| 操作权限
---|---|---
admin | 123456| 演示环境无法进行修改删除操作

## 📌 版本说明

|             版本名称             |                   说明                    | 地址
|:----------------------------:|:---------------------------------------:| :---: |
|   Yii2.x+Layui混编专业版    |  采用Yii2.x、Layui、MySQL等框架研发的混编专业版本  | https://gitee.com/rxthinkcmf/RXThinkCMF_Yii2
|   Yii2.x+Layui混编旗舰版    |  采用Yii2.x、Layui、MySQL等框架研发的混编旗舰版本  | https://gitee.com/rxthinkcmf/RXThinkCMF_Yii2_PRO
| Yii2.x+Vue+ElementUI旗舰版  | 采用Yii2.x、Vue、ElementUI等框架研发前后端分离版本  | https://gitee.com/rxthinkcmf/RXThinkCMF_Yii2_EleVue
| Yii2.x+Vue+AntDesign旗舰版  | 采用Yii2.x、Vue、AntDesign等框架研发前后端分离版本  | https://gitee.com/rxthinkcmf/RXThinkCMF_Yii2_AntdVue

## 📚 核心组件

+ 单图上传组件
```
{upload:image name="avatar|头像|90x90|建议上传尺寸450x450" value="isset($info['avatar_url']) ? $info['avatar_url'] : ''"}
```
+ 多图上传组件
```
{upload:album name="imgs|图集|90x90|20|建议上传尺寸450x450" value="isset($info['imgsList']) ? $info['imgsList'] : []"}
```
+ 下拉选择组件
```
{common:select param="gender|1|性别|name|id" data="$genderList" value="isset($info['gender']) ? $info['gender'] : 1"}
```
+ 单选按钮组件
```
{common:radio name="city_id|name|id" data="$cityList" value="isset($info['value']) ? $info['value'] : 0"}
```
+ 复选框组件
```
{checkbox:select param="type|name|id" data="1=云计算,2=数据库,3=大数据" value="isset($vo['type']) ? $vo['type'] : ''"}
```
+ 城市选择组件
```
{city:select value="isset($info['province_name'])?$info['province_name']: '',isset($info['city_name'])?$info['city_name']:'',isset($info['district_name'])?$info['district_name']:''" limit="3"}
```
+ 开关组件
```
{common:switch name="status" title="是|否" value="isset($info['status']) ? $info['status'] : 1"}
```
+ 日期组件
```
{date:select param="entry_date|入职时间|date" value="$info.format_entry_date|default=''"}
```
+ 图标组件
```
{icon:picker name="icon" value="isset($info['icon']) ? $info['icon'] : 'layui-icon-component'"}
```
+ 穿梭组件
```
{transfer:select param="func|0|全部节点,已赋予节点|name|id|220x350" data="1=列表,5=添加,10=修改,15=删除,20=详情,25=状态,30=批量删除,35=添加子级,40=全部展开,45=全部折叠,50=导入数据,55=导出数据,60=设置权限,65=重置密码" value="isset($info['funcList'])?$info['funcList']:[]"}
```

## ✨  特别鸣谢
感谢[Yii2](https://www.yiiframework.com/)、[Layui](https://www.layuion.com/)等优秀开源项目。

## 📚 版权信息

软件版权和最终解释权归RXThinkCMF研发团队所有，商业版使用需授权，未授权禁止恶意传播和用于商业用途，否则将追究相关人的法律责任。

本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2018~2022 [rxthink.cn](https://www.rxthink.cn) All rights reserved。

更多细节参阅 [LICENSE](LICENSE)